﻿About
=====
Dashboard uses Django, which is a python Web framework: https://www.djangoproject.com/.
It presents vulnerability list and scanning results, and reports about available updates/upgrades of vulnerable libraries.

Installation
=====

Create a virtualenv with:
::
	virtualenv /path/to/env
	source /path/to/env/bin/activate

Install requirements:
::
	pip install -r /path/to/specs-enforcement-sva-dashboard/dashboard/requirements.txt
	sudo zypper install redis
	sudo zypper install nodejs
	sudo npm install -g less

Migrate Django database:
::
	/path/to/specs-enforcement-sva-dashboard/dashboard/python manage.py migrate

Install postgresql:
::
	zypper install postgresql-devel
	zypper install postgresql
	zypper install postgresql-contrib
        zypper install python-devel

	service postgresql start
	sudo -u postgres psql -c "ALTER USER postgres PASSWORD 'postgres';“
	sudo -u postgres createdb dashboard
	sudo -u postgres createuser -P dashboard 

	you will be prompted for password, password should be dashboard

	sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE dashboard TO dashboard;“

Run redis server with:
:: 
	redis-server

Test redis connection
::
	redis-cli ping
	
Run celery worker:
::
	cd /path/to/specs-enforcement-sva-dashboard/dashboard
	celery -A dashboard_web worker -B --loglevel=INFO --concurrency=10

You will also need to open port, if you want dashboard to be accessible from other computers.
::
		SuSEfirewall2 open EXT TCP 8000
		SuSEfirewall2 stop
		SuSEfirewall2 start
Navigate to django directory and run django server:
::
	cd /path/to/specs-enforcement-sva-dashboard/dashboard
	python manage.py runserver 0.0.0.0:8000
	
Django server is now ready to receive reports and display their data.
Open http://localhost:8000/ in any web browser.