from django.test import TestCase
from django.test import Client
from dashboard.models import ScanningReport, UpgradeReport, Cvss, SVAInformation, VirtualMachine, OvalReport


class ReportTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.ip = '127.0.0.1'

    def test_scan_report_post(self):
        with open('tests/test_files/results.xml') as fp:
            response = self.client.post('/scanning_report_post/', {'file': fp})
        self.assertEqual(response.status_code, 200)

    def test_oval_report(self):
        with open('tests/test_files/oval.xml') as fp:
            response = self.client.post('/oval_report_post/', {'file': fp})
        self.assertEqual(response.status_code, 200)

    def test_upgrade_report(self):
        file1 = open('tests/test_files/results.xml')
        file2 = open('tests/test_files/available_updates.json')
        response = self.client.post('/upgrade_report_post/', {'file1': file1, 'file2': file2})
        self.assertEqual(response.status_code, 200)

    def test_scan_report_available(self):
        ScanningReport.objects.generate_report(open('tests/test_files/results.xml').read(), 1, self.ip)
        report_count = ScanningReport.objects.filter().count()
        self.assertEqual(report_count, 1)

    def test_oval_report_available(self):
        OvalReport.objects.generate_report(open('tests/test_files/oval.xml').read(), 1, self.ip)
        report_count = OvalReport.objects.filter().count()
        self.assertEqual(report_count, 1)

    def test_up_report_available(self):
        UpgradeReport.objects.generate_report(open('tests/test_files/results.xml').read(), open('tests/test_files/available_updates.json').read(), 1, self.ip)
        report_count = UpgradeReport.objects.filter().count()
        self.assertEqual(report_count, 1)
