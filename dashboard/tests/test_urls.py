from django.test import TestCase
from django.test import Client
from dashboard.models import ScanningReport, UpgradeReport, Cvss, SVAInformation, VirtualMachine, OvalReport
from dashboard import tasks


class UrlsTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.ip = '127.0.0.1'

    def generate_reports(self):
        tasks._add_virtual_machine(self.ip)
        ScanningReport.objects.generate_report(open('tests/test_files/results.xml').read(), 1, self.ip)
        OvalReport.objects.generate_report(open('tests/test_files/oval.xml').read(), 1, self.ip)
        UpgradeReport.objects.generate_report(open('tests/test_files/results.xml').read(), open('tests/test_files/available_updates.json').read(), 1, self.ip)

    def test_report_url(self):
        self.generate_reports()
        id = VirtualMachine.objects.filter().order_by('-id')[0].id
        response = self.client.get('/reports/%s/' % id)
        self.assertEqual(response.status_code, 200)

    def test_wrong_virtual_machine_url(self):
        self.generate_reports()
        id = VirtualMachine.objects.filter().order_by('-id')[0].id + 1
        response = self.client.get('/reports/%s/' % id)
        self.assertRedirects(response, '/virtual_machines/', status_code=302, target_status_code=200)

    def test_oval_file_url(self):
        self.generate_reports()
        id = VirtualMachine.objects.filter().order_by('-id')[0].id
        response = self.client.get('/files/%s/oval' % id)
        self.assertEqual(response.status_code, 200)

    def test_scanning_report_file_url(self):
        self.generate_reports()
        id = VirtualMachine.objects.filter().order_by('-id')[0].id
        response = self.client.get('/files/%s/scanning_report' % id)
        self.assertEqual(response.status_code, 200)

    def test_upgrade_report_file_url(self):
        self.generate_reports()
        id = VirtualMachine.objects.filter().order_by('-id')[0].id
        response = self.client.get('/files/%s/upgrade_report' % id)
        self.assertEqual(response.status_code, 200)