from xml.etree import cElementTree as ET
import models


class ScanningReportGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate_scanning_report(scanning_report, result_xml):
        context = ET.iterparse(result_xml, events=("end", ))
        cves = list()
        definitions = list()
        for event, elem in context:
            if event == 'end':
                if elem.tag == tag_ovals('definition'):
                    metadata = elem.find(tag_ovals('metadata'))
                    description = metadata.find(tag_ovals('description')).text.strip()
                    cve = metadata.find(tag_ovals('title')).text
                    definition_id = elem.attrib['id']
                    elem.clear()
                    try:
                        CVE = models.Cve.objects.filter(cve=cve).exclude(cvss__isnull=True)[0]
                    except IndexError, models.Cve.DoesNotExist:
                        CVE = models.Cve(cve=cve)
                        cves.append(CVE)

                    try:
                        models.Definition.objects.get(definition_id=definition_id, scan_report=scanning_report)
                    except models.Definition.DoesNotExist:
                        definition = models.Definition(definition_id=definition_id, scan_report=scanning_report, description=description, cve=CVE)
                        definitions.append(definition)

        models.Cve.objects.bulk_create(cves, batch_size=1000)
        models.Definition.objects.bulk_create(definitions, batch_size=1000)
        del cves[:], definitions[:]
        del context
        ScanningReportGenerator._append_packages(result_xml, scanning_report)

    @staticmethod
    def _append_packages(result_xml, scanning_report):
        context = ET.iterparse(result_xml, events=("end", ))
        for event, elem in context:
            if event == 'end':
                if elem.tag == tag_results('definition'):
                    id = elem.attrib['definition_id']
                    vulnerability_result = elem.attrib['result']
                    try:
                        d = models.Definition.objects.get(definition_id=id, scan_report=scanning_report)
                        d.vulnerability_result = vulnerability_result == 'true'
                        d.save()
                    except models.Definition.DoesNotExist:
                        continue
                    # if vulnerability_result == 'true':
                    #     criteria = definition.find(tag_results('criteria'))
                    #     for criterion in criteria.iter(tag_results('criterion')):
                    #         result = criterion.attrib['result']
                    #         if result == 'true':
                    #             package = ScanningReportGenerator._get_package(criterion, result_xml)
                    #             try:
                    #                 p = models.Package.objects.get(package_id=package['item_id'])
                    #             except models.Package.DoesNotExist:
                    #                 p = models.Package.objects.create(package_id=package['item_id'])
                    #             p.name = package['name']
                    #             p.version = package['version']
                    #             p.definitions.add(d)
                    #             p.save()
                    elem.clear()
        del context

    @staticmethod
    def _get_package(criterion, result_xml):
        package = dict()
        test_ref = criterion.attrib['test_ref']
        context = ET.iterparse(result_xml, events=("start", "end"))
        for event, elem in context:
            if event == 'end':
                if elem.tag == tag_results('results'):
                    tests = elem.find(tag_results('system')).find(tag_results('tests'))
                    for test in tests.iter(tag_results('test')):
                        if test.attrib['test_id'] == test_ref:
                            item_id = test.find(tag_results('tested_item')).attrib['item_id']
                            tag_prefix = '{http://oval.mitre.org/XMLSchema/oval-system-characteristics-5}'
                            oval_system = elem.find(tag_results('system')).find(tag_prefix + 'oval_system_characteristics')
                            packages = oval_system.find(tag_prefix + 'system_data')
                            for item in packages:
                                if item.attrib['id'] == item_id:
                                    tag_prefix = '{http://oval.mitre.org/XMLSchema/oval-system-characteristics-5#linux}'
                                    name = item.find(tag_prefix + 'name').text
                                    version = item.find(tag_prefix + 'version').text
                                    package['item_id'] = item_id
                                    package['name'] = name
                                    package['version'] = version
                                    return package
                    elem.clear()
        del context


def tag_ovals(tag_name):
    return '{http://oval.mitre.org/XMLSchema/oval-definitions-5}' + tag_name


def tag_results(tag_name):
    return '{http://oval.mitre.org/XMLSchema/oval-results-5}' + tag_name
