import logging
from django.views.generic import TemplateView, View
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from models import ScanReport, UpgradeReport, Cve, SVAInformation, VirtualMachine, ListReport, ExtendedScanReport, \
    Report
from tasks import generate_scanning_report, generate_upgrade_report, generate_oval_report, generate_openvas_report
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.http import StreamingHttpResponse
from django.core.servers.basehttp import FileWrapper
from common.utils import save_file
import os
import json
import constants
log = logging.getLogger(__name__)


def get_ip_address(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        return x_forwarded_for.split(',')[-1].strip()
    return request.META.get('REMOTE_ADDR')


def get_slo_by_name(plan, *names):
    slos = []
    try:
        for slo in plan['raw_data']['slos']:
            for name in names:
                if name in slo['metric_id']:
                    slos += [slo]
    except KeyError:
        return {}
    return slos


class IndexView(TemplateView):
    template_name = 'dashboard/index.html'

    def dispatch(self, request, *args, **kwargs):
        return redirect('virtual_machines')


class VirtualMachineView(TemplateView):
    template_name = 'dashboard/virtual_machines.html'

    def get_context_data(self, **kwargs):
        context = super(VirtualMachineView, self).get_context_data(**kwargs)
        vms = VirtualMachine.objects.all_with_reports()
        virtual_machines = []
        for virtual_machine in vms:
            ip = virtual_machine.ip
            virtual_machine_id = virtual_machine.id

            oval_report = ListReport.objects.get_last_finished_report(virtual_machine)
            scanning_report = ScanReport.objects.get_last_finished_report(virtual_machine)
            upgrade_report = UpgradeReport.objects.get_last_finished_report(virtual_machine)

            sva_information = SVAInformation.objects.get(virtual_machine=virtual_machine)
            plan = json.loads(sva_information.plan.read())

            basic_scan, list_update, up_report = get_slo_by_name(plan, 'basic_scan', 'list_update', 'up_report')

            virtual_machine = {'ip': ip,
                               'vulnerabilities_length': scanning_report.get_vulnerability_size(),
                               'max_cvss_score': scanning_report.get_max_cvss(),
                               'is_online': virtual_machine.is_online,
                               'id': virtual_machine_id,
                               'plan': plan.get('raw_data', {}),
                               'basic_scan': basic_scan,
                               'list_update': list_update,
                               'up_report': up_report,
                               'scanning_report': scanning_report,
                               'upgrade_report': upgrade_report,
                               'list_report': oval_report,
                               'basic_scan_status': scanning_report.get_status(basic_scan['value']),
                               'list_update_status': oval_report.get_status(list_update['value']),
                               'up_report_status': upgrade_report.get_status(up_report['value']),
                               'sva_information': sva_information,
                               'current_repository': sva_information.current_repository,
                               'plan_url': reverse('file', kwargs={'virtual_machine_number': virtual_machine_id,
                                                                   'name': 'plan'}),
                               'report_url': reverse('reports',
                                                     kwargs={'virtual_machine_number': virtual_machine_id})
                               }
            virtual_machines.append(virtual_machine)
            context['current_repository'] = sva_information.current_repository

        context['virtual_machines_json'] = virtual_machines
        context['dashboard_url'] = self.request.build_absolute_uri('/')
        return context


class Reports(TemplateView):
    template_name = 'dashboard/reports/report.html'

    def __init__(self):
        super(Reports, self).__init__()
        self.virtual_machine = None

    def get(self, request, *args, **kwargs):
        try:
            self.virtual_machine = VirtualMachine.objects.get(id=kwargs['virtual_machine_number'])
        except VirtualMachine.DoesNotExist:
            return redirect('virtual_machines')
        return super(Reports, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Reports, self).get_context_data(**kwargs)

        oval_report = ListReport.objects.get_last_finished_report(self.virtual_machine)
        scanning_report = ScanReport.objects.get_last_finished_report(self.virtual_machine)
        upgrade_report = UpgradeReport.objects.get_last_finished_report(self.virtual_machine)
        # extended_scan_report = ExtendedScanReport.objects.get_last_finished_report(self.virtual_machine)
        # try:
        #     extended_scan_report_json = ast.literal_eval(extended_scan_report.raw_content.read())
        #     context['extended_scan_report'] = extended_scan_report_json
        #     context['extended_scan_rows'] = range(' '.join(extended_scan_report_json['data']).count('scan_nvt_version'))
        # except:
        #     pass

        # Sort by cvss descending order
        context['scanning_report'] = scanning_report.definition_set.all().exclude(cve__cvss__isnull=True).order_by(
            '-cve__cvss', 'definition_id')
        context['upgrade_report'] = upgrade_report.upgradeablepackage_set.all()
        try:
            sva_information = SVAInformation.objects.get(virtual_machine=self.virtual_machine)
            context['scanning_report_age'] = scanning_report.time_added
            context['upgrade_report_age'] = upgrade_report.time_added
            context['oval_age'] = oval_report.time_added
            context['list_update_frequency'] = sva_information.list_update_frequency
            context['upgrade_report_frequency'] = sva_information.upgrade_report_frequency
            context['basic_scan_frequency'] = sva_information.basic_scan_frequency
        except SVAInformation.DoesNotExist:
            pass
        context['ip'] = self.virtual_machine.ip
        return context


class FileView(TemplateView):
    def response_download(self, file):
        the_file = file.path
        filename = os.path.basename(the_file)
        chunk_size = 8192
        response = StreamingHttpResponse(FileWrapper(open(the_file), chunk_size),
                                         content_type='application/force-download')
        response['Content-Length'] = os.path.getsize(the_file)
        response['Content-Disposition'] = "attachment; filename=%s" % filename
        return response

    def get(self, request, *args, **kwargs):
        virtual_machine = VirtualMachine.objects.get(id=kwargs['virtual_machine_number'])
        name = kwargs['name']
        if name == 'oval':
            return self.response_download(ListReport.objects.get_last_finished_report(virtual_machine).raw_content)
        if name == 'scanning_report':
            return self.response_download(ScanReport.objects.get_last_finished_report(virtual_machine).raw_content)
        if name == 'upgrade_report':
            return self.response_download(UpgradeReport.objects.get_last_finished_report(virtual_machine).raw_content)
        if name == 'extended_scan_report':
            return self.response_download(ExtendedScanReport.objects.get_last_finished_report(virtual_machine).raw_content)
        if name == 'plan':
            return HttpResponse(
                json.dumps(json.loads(SVAInformation.objects.get(virtual_machine=virtual_machine).plan.read()), indent=2),
                content_type='application/json')
        return redirect('dashboard_index')


class PostReceiver(TemplateView):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PostReceiver, self).dispatch(request, *args, **kwargs)


class OvalReportReceiver(PostReceiver):
    """
    Receives oval vulnerability list,
    saves it to database
    """

    def post(self, request, *args, **kwargs):
        sender_ip = get_ip_address(request)
        if 'file' in request.FILES:
            oval_file_content = request.FILES['file'].read()
            oval_file = save_file(constants.OVAL_TEMP, oval_file_content)
            generate_oval_report.delay(oval_file.name, sender_ip)
            return HttpResponse()


class ScanningReportReceiver(PostReceiver):
    """
    Receives scanning report,
    generates readable report
    """

    def post(self, request, *args, **kwargs):  # Receives xml, generates scanning reports
        sender_ip = get_ip_address(request)
        if 'file' in request.FILES:
            scanning_results = request.FILES['file'].read()
            scanning_file = save_file(constants.RESULTS_TEMP, scanning_results)
            generate_scanning_report.delay(scanning_file.name, sender_ip)
            return HttpResponse()


class UpgradeReportReceiver(PostReceiver):
    """
    Receives scanning report and installed packages with available updates on virtual machines,
    generates readable report with available updates for affected packages
    """

    def post(self, request, *args, **kwargs):
        sender_ip = get_ip_address(request)
        if 'file1' in request.FILES and 'file2' in request.FILES:
            scanning_results = request.FILES['file1'].read()
            available_updates = request.FILES['file2'].read()
            scanning_file = save_file(constants.RESULTS_TEMP, scanning_results)
            upgrade_file = save_file(constants.UPGRADE_TEMP, available_updates)
            generate_upgrade_report.delay(scanning_file.name, upgrade_file.name, sender_ip)
            return HttpResponse()


class OpenVasReportReceiver(PostReceiver):
    """
    Receives reports from OpenVAS client
    """

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        if data['object'] == 'object':  # It means we have a full report
            generate_openvas_report.delay(data)
            return HttpResponse()
        return HttpResponse()


class SVAPlan(PostReceiver):
    """
    Receives plan,
    parses valuable information and saves file
    """

    def post(self, request, *args, **kwargs):
        sender_ip = get_ip_address(request)
        plan = request.body
        SVAInformation.objects.parse_plan(sender_ip, plan)
        return HttpResponse()


class SVAInformationReceiver(PostReceiver):
    """
    Receives and saves information about SVA such as frequencies, ...
    """

    def post(self, request, *args, **kwargs):
        sender_ip = get_ip_address(request)
        data = json.loads(request.body)
        logging.info(data)
        SVAInformation.objects.create_from_json(sender_ip, data)
        return HttpResponse()
