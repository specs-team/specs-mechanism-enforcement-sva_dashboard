from django import template
from django.utils import timezone
register = template.Library()


@register.filter(name='format_value_with_default')
def format_value_with_default(value, default):
    if value:
        return str(value) + 'h'
    return default


@register.filter(name='file_age')
def file_age(value, default):
    if value:
        return _format_seconds((timezone.now() - value).total_seconds())
    return default


def _format_seconds(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return "%d:%02d:%02d" % (h, m, s)


@register.filter(name='keyvalue')
def keyvalue(dict, key):
    try:
        return dict[key]
    except KeyError:
        return ''
