from django import template
from dashboard.models import Report

register = template.Library()


@register.filter
def get_verbose_name(obj, field_name):
    return obj._meta.get_field(field_name).verbose_name


@register.filter
def get_field_value(obj, field_name):
    return getattr(obj, field_name, None)


@register.filter
def get_total_seconds(obj):
    if isinstance(obj, Report):
        seconds = obj.get_total_seconds()
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        return "%d:%02d:%02d" % (h, m, s)
    raise TypeError


@register.filter
def get_status(obj, metric):
    if isinstance(obj, Report) and 'value' in metric:
        return obj.get_status(metric['value'])
    raise TypeError
