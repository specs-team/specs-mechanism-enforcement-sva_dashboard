import datetime
import json
from constants import *
import dicttoxml
import pytz
from celery.task.control import revoke
from django.core.files import File
from django.core.files.base import ContentFile
from django.db import models as django_models
from django.db import transaction
from xml.etree import cElementTree as ET
import models


class ReportManager(django_models.Manager):
    def clear_old_reports(self, sender_ip):
        finished_reports = self.filter(status=models.Report.FINISHED, virtual_machine__ip=sender_ip).order_by(
            'time_added')
        if finished_reports.count() > 1:
            for report in finished_reports[:finished_reports.count() - 1]:
                report.delete()

    def terminate_generating_report(self, sender_ip):
        generating_reports = self.filter(status=models.Report.GENERATING, virtual_machine__ip=sender_ip).order_by(
            'time_added')
        if generating_reports.count() > 3:
            for report in generating_reports[:generating_reports.count() - 1]:
                report.status = models.Report.TERMINATED
                report.save()
                revoke(report.task_id, terminate=True)

    def get_last_finished_report(self, virtual_machine):
        try:
            report = self.filter(status=models.Report.FINISHED, virtual_machine=virtual_machine).order_by('-id')[0]
            return report
        except IndexError:
            raise models.Report.DoesNotExist

    def is_report_generating(self, ip):
        try:
            report = self.filter(ip=ip).order_by('-id')[0]
            return report.status == models.Report.GENERATING
        except IndexError:
            return False

    def delete_reports(self, ip):
        for report in self.filter(ip=ip):
            report.delete()


class ListReportManager(ReportManager):
    def generate_report(self, oval_file_path, task_id, sender_ip):
        virtual_machine, created = models.VirtualMachine.objects.get_or_create(ip=sender_ip)
        report = self.create(status=models.ListReport.FINISHED, task_id=task_id, virtual_machine=virtual_machine)
        oval_file = open(oval_file_path)
        report.raw_content.save('file.xml', File(oval_file))
        oval_file.close()
        report.save()


class UpReportManager(ReportManager):
    def generate_report(self, results_file_path, available_updates_file_path, task_id, sender_ip):
        virtual_machine, created = models.VirtualMachine.objects.get_or_create(ip=sender_ip)
        report = self.create(status=models.UpgradeReport.GENERATING, task_id=task_id, virtual_machine=virtual_machine)
        report.save()
        report_json = report.generate_report(results_file_path, json.loads(open(available_updates_file_path).read()))

        xml = dicttoxml.dicttoxml(report_json, attr_type=False, custom_root='available_updates')
        report.raw_content.save('file.xml', ContentFile(xml))
        report.status = models.UpgradeReport.FINISHED
        report.save()


class ScanReportManager(ReportManager):
    def generate_report(self, results_file_path, task_id, sender_ip):
        virtual_machine, created = models.VirtualMachine.objects.get_or_create(ip=sender_ip)
        scanning_report = self.create(status=models.ScanReport.GENERATING, task_id=task_id,
                                      virtual_machine=virtual_machine)
        scanning_file = open(results_file_path)
        scanning_report.raw_content.save('file.xml', File(scanning_file))
        scanning_report.save()
        scanning_report.generate_report(scanning_report.raw_content.path)
        scanning_report.status = models.ScanReport.FINISHED
        scanning_report.save()


class ExtendedScanReportManager(ReportManager):
    def generate_report(self, results, task_id, ip):
        virtual_machine, created = models.VirtualMachine.objects.get_or_create(ip=ip)
        report = self.create(status=models.ExtendedScanReport.GENERATING, task_id=task_id,
                             virtual_machine=virtual_machine)
        report.raw_content.save('file.json', ContentFile(results))
        report.status = models.ExtendedScanReport.FINISHED
        report.save()


class SVAInformationManager(django_models.Manager):
    @transaction.atomic
    def create_from_json(self, sender_ip, data):
        if data['data'].get('ip', None):
            sender_ip = data['data']['ip']
        virtual_machine, created = models.VirtualMachine.objects.get_or_create(ip=sender_ip)
        info, created = self.get_or_create(virtual_machine=virtual_machine)
        value = data['data']['value']
        if not isinstance(value, bool):
            try:
                value = datetime.datetime.fromtimestamp(value, tz=pytz.UTC)
            except TypeError:
                pass
        measurement = data['type']
        if hasattr(info, measurement):
            setattr(info, measurement, value)
        info.save()

    def parse_plan(self, sender_ip, plan):
        plan = json.loads(plan)
        slos = plan['raw_data']['slos']
        virtual_machine, created = models.VirtualMachine.objects.get_or_create(ip=sender_ip)
        info, created = self.get_or_create(virtual_machine=virtual_machine)
        info.plan.save('file.json', ContentFile(json.dumps(plan)))
        for slo in slos:
            value = slo['value']
            metric_id = slo['metric_id']
            if metric_id == 'extended_scan_frequency':
                info.extended_scan_frequency = value
            if metric_id == BASIC_SCAN_METRIC:
                info.basic_scan_frequency = value
            if metric_id == LIST_UPDATE_METRIC:
                info.list_update_frequency = value
            if metric_id == UP_REPORT_METRIC:
                info.upgrade_report_frequency = value
        info.save()


class CveManager(django_models.Manager):
    def create_from_xml(self, xml):
        insert_list = list()
        context = ET.iterparse(xml, events=("start", "end"))
        for event, elem in context:
            if event == 'start':
                if elem.tag == '{http://nvd.nist.gov/feeds/cve/1.2}entry':
                    cve = elem.attrib['name']
                    score = self._get_score(elem)
                    elem.clear()
                    insert_list.append(models.Cve(cve=cve, cvss=score))
        del context
        self.bulk_create(insert_list, batch_size=1000)

    def update_from_xml(self, xml):
        context = ET.iterparse(xml, events=("start", "end"))
        for event, elem in context:
            if event == 'start':
                if elem.tag == '{http://nvd.nist.gov/feeds/cve/1.2}entry':
                    try:
                        cve = elem.attrib['name']
                        score = self._get_score(elem)
                        elem.clear()
                        try:
                            cvss = self.filter(cve=cve)[0]
                        except IndexError, models.Cve.DoesNotExist:
                            cvss = self.create(cve=cve)
                        cvss.cvss = score
                        cvss.save()
                    except TypeError:
                        pass

    def _get_score(self, elem):
        try:
            return float(elem.attrib['CVSS_score'])
        except (KeyError, AttributeError) as e:
            return -1

    def get_cvss_from_cve(self, cve):
        try:
            return self.filter(cve=cve)[0].cvss
        except IndexError, models.Cve.DoesNotExist:
            return -1


class VirtualMachineManager(django_models.Manager):
    def all_with_reports(self):
        vms = self.all()
        vms_with_reports = list()
        for vm in vms:
            try:
                models.ListReport.objects.get_last_finished_report(vm)
                models.ScanReport.objects.get_last_finished_report(vm)
                models.UpgradeReport.objects.get_last_finished_report(vm)
                vms_with_reports.append(vm)
            except models.Report.DoesNotExist:
                pass
        return vms_with_reports
