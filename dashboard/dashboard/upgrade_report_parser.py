from xml.etree import cElementTree as ET
import models
import logging
log = logging.getLogger(__name__)


class UpgradeReportGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate_upgrade_report(upgrade_report, result_xml, available_updates):
        upgradable_packages = list()
        context = ET.iterparse(result_xml, events=("start", "end"))
        tag_prefix = '{http://oval.mitre.org/XMLSchema/oval-system-characteristics-5#linux}'
        for event, elem in context:
            if event == 'start':
                try:
                    if elem.tag == tag_prefix + 'rpminfo_item':
                        vulnerable_package = elem.find(tag_prefix + 'name').text
                        for p in available_updates:
                            if p['name'].lower() == vulnerable_package.lower():
                                p['id'] = elem.attrib['id']
                                try:
                                    models.UpgradeablePackage.objects.get(package_id=p['id'], upgrade_report=upgrade_report)

                                except models.UpgradeablePackage.DoesNotExist:
                                    models.UpgradeablePackage.objects.create(package_id=p['id'], name=p['name'],
                                                                             current_version=p['current_version'],
                                                                             available_version=p['available_version'],
                                                                             upgrade_report=upgrade_report).save()
                                upgradable_packages.append(p)

                                try:
                                    definition_package = models.Package.objects.get(package_id=p['id'])
                                    definition_package.available_version = p['available_version']
                                    definition_package.save()
                                except models.Package.DoesNotExist:
                                    models.Package.objects.create(package_id=p['id'], name=p['name'],
                                                                  version=p['current_version'],
                                                                  available_version=p['available_version']).save()
                except AttributeError:
                    log.warn("Error parsing %s" % elem.tag)
            elem.clear()
        return upgradable_packages


def tag(tag_name):
    return '{http://oval.mitre.org/XMLSchema/oval-definitions-5}' + tag_name


def tag_results(tag_name):
    return '{http://oval.mitre.org/XMLSchema/oval-results-5}' + tag_name
