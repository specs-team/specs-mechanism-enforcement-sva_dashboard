from models import UpgradeReport, VirtualMachine, ListReport, ExtendedScanReport, ScanReport, Cve
from billiard.exceptions import Terminated
from datetime import date
from django.conf import settings
from celery.task import Task
from dashboard_web.celery import app
import logging
import os
import urllib
import zipfile
import glob
import gc
import socket
log = logging.getLogger(__name__)


@app.task(throws=(Terminated,))
def generate_scanning_report(scanning_results, sender_ip):
    ScanReport.objects.generate_report(scanning_results, generate_scanning_report.request.id, sender_ip)


@app.task(throws=(Terminated,))
def generate_upgrade_report(results, available_updates, sender_ip):
    UpgradeReport.objects.generate_report(results, available_updates, generate_upgrade_report.request.id, sender_ip)


@app.task(throws=(Terminated,))
def generate_oval_report(oval, sender_ip):
    ListReport.objects.generate_report(oval, generate_oval_report.request.id, sender_ip)


@app.task(throws=(Terminated,))
def generate_openvas_report(result):
    ip = result['data']['host_0']
    ExtendedScanReport.objects.generate_report(result, generate_openvas_report.request.id, ip)


class InitialCVSSDownload(Task):
    """
        Downloads initial cvss when django is ran
    """

    def run(self):
        if Cve.objects.count() > 0:
            return
        for year in range(2002, date.today().year + 1):
            log.info("Downloading cvss files: %s" % year)
            url = 'https://nvd.nist.gov/download/nvdcve-%s.xml.zip' % year
            unzip(url, year)

        xml_files = glob.glob(settings.MEDIA_ROOT + '/cvss' + '/*.xml')
        for xml_file in xml_files:
            Cve.objects.create_from_xml(xml_file)
        log.info("CVSS download completed")


@app.task(throws=(Terminated, IOError, OSError,))
def update_cvss_database():
    url = 'https://nvd.nist.gov/download/nvdcve-Recent.xml.zip'
    unzip(url, 'recent')
    xml_file = settings.MEDIA_ROOT + '/cvss' + '/nvdcve-recent.xml'
    Cve.objects.update_from_xml(xml_file)


def unzip(url, filename):
    directory = settings.MEDIA_ROOT + '/cvss/'
    if not os.path.exists(directory):
        os.makedirs(directory)
    file_path = directory + 'nvdcve-%s.zip' % filename
    urllib.urlretrieve(url, file_path)

    with zipfile.ZipFile(file_path, "r") as z:
        z.extractall(directory)
    os.remove(file_path)


@app.task
def delete_old_reports():
    # TODO: Delete old reports, so we don't use up all disk space?
    pass


@app.task
def check_virtual_machines():
    for vm in VirtualMachine.objects.all():
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((vm.ip, 22))
            vm.is_online = True
        except socket.error as e:
            vm.is_online = False
        s.close()
