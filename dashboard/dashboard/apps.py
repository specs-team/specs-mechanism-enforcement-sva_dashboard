from django.apps import AppConfig
from tasks import InitialCVSSDownload


class DashboardConfig(AppConfig):
    """
    When django server is started, ready method is executed
    """
    name = 'dashboard'

    def ready(self):
        initial_cvss_download = InitialCVSSDownload()
        initial_cvss_download.delay()
