from django.conf.urls import patterns, url
from dashboard.views import IndexView, Reports, VirtualMachineView, \
    ScanningReportReceiver, UpgradeReportReceiver, SVAInformationReceiver, FileView, SVAPlan, OvalReportReceiver, \
    OpenVasReportReceiver
urlpatterns = patterns('',
                       url(r'^$', IndexView.as_view(), name='dashboard_index'),
                       url(r'^virtual_machines/$', VirtualMachineView.as_view(), name='virtual_machines'),
                       url(r'^(?P<virtual_machine_number>[0-9]+)/reports/$', Reports.as_view(), name='reports'),
                       url(r'^scanning_report_post/$', ScanningReportReceiver.as_view(), name='upgrade_report_receiver'),
                       url(r'^upgrade_report_post/$', UpgradeReportReceiver.as_view(), name='upgrade_report_receiver'),
                       url(r'^oval_report_post/$', OvalReportReceiver.as_view(), name='oval_report_receiver'),
                       url(r'^openvas_report_post/$', OpenVasReportReceiver.as_view(), name='openvas_report_receiver'),
                       url(r'^sva_information/$', SVAInformationReceiver.as_view(), name='sva_information'),
                       url(r'^files/(?P<virtual_machine_number>[0-9]+)/(?P<name>.*)$', FileView.as_view(), name='file'),
                       url(r'^plan/$', SVAPlan.as_view(), name='plan'),
                       )