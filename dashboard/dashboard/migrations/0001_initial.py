# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import dashboard.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cve',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cve', models.CharField(max_length=128, db_index=True)),
                ('cvss', models.FloatField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Definition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('definition_id', models.CharField(max_length=512, db_index=True)),
                ('description', models.CharField(max_length=2048)),
                ('vulnerability_result', models.NullBooleanField()),
                ('cve', models.ForeignKey(to='dashboard.Cve', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('package_id', models.CharField(max_length=128)),
                ('name', models.CharField(max_length=1024)),
                ('version', models.CharField(max_length=128)),
                ('available_version', models.CharField(max_length=128)),
                ('definitions', models.ManyToManyField(to='dashboard.Definition', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_added', models.DateTimeField(auto_now_add=True)),
                ('task_id', models.CharField(max_length=200)),
                ('status', models.SmallIntegerField(default=0, choices=[(0, b'Generating'), (1, b'Finished'), (2, b'Terminated')])),
                ('raw_content', models.FileField(null=True, upload_to=dashboard.models.raw_content)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ListReport',
            fields=[
                ('report_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='dashboard.Report')),
            ],
            options={
            },
            bases=('dashboard.report',),
        ),
        migrations.CreateModel(
            name='ExtendedScanReport',
            fields=[
                ('report_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='dashboard.Report')),
            ],
            options={
            },
            bases=('dashboard.report',),
        ),
        migrations.CreateModel(
            name='ScanReport',
            fields=[
                ('report_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='dashboard.Report')),
            ],
            options={
            },
            bases=('dashboard.report',),
        ),
        migrations.CreateModel(
            name='SVAInformation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('list_age_sva_msr2', models.DateTimeField(null=True, verbose_name=b'List age')),
                ('report_basic_age_sva_msr1', models.DateTimeField(null=True, verbose_name=b'Basic scan age')),
                ('up_report_age_sva_msr4', models.DateTimeField(null=True, verbose_name=b'Upgrade report age')),
                ('current_repository', models.CharField(max_length=100, null=True, verbose_name=b'Current repository')),
                ('list_update_frequency', models.IntegerField(null=True, verbose_name=b'List update frequency')),
                ('basic_scan_frequency', models.IntegerField(null=True, verbose_name=b'Basic scan frequency')),
                ('extended_scan_frequency', models.IntegerField(null=True, verbose_name=b'Extended scan frequency')),
                ('upgrade_report_frequency', models.IntegerField(null=True, verbose_name=b'Upgrade report frequency')),
                ('list_availability_sva_msr7', models.NullBooleanField(verbose_name=b'List available')),
                ('up_report_availability_sva_msr10', models.NullBooleanField(verbose_name=b'Upgrade report available')),
                ('scan_report_availability_sva_msr9', models.NullBooleanField(verbose_name=b'Scan report available')),
                ('repository_availability_sva_msr6', models.NullBooleanField(verbose_name=b'Repository available')),
                ('scanner_availability_sva_msr8', models.NullBooleanField(verbose_name=b'Scanner available')),
                ('plan', models.FileField(upload_to=dashboard.models.plan, null=True, verbose_name=b'Plan')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UpgradeablePackage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('package_id', models.CharField(max_length=128)),
                ('name', models.CharField(max_length=1024)),
                ('current_version', models.CharField(max_length=128)),
                ('available_version', models.CharField(max_length=128)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UpgradeReport',
            fields=[
                ('report_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='dashboard.Report')),
            ],
            options={
            },
            bases=('dashboard.report',),
        ),
        migrations.CreateModel(
            name='VirtualMachine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.IPAddressField()),
                ('is_online', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='upgradeablepackage',
            name='upgrade_report',
            field=models.ForeignKey(to='dashboard.UpgradeReport'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='upgradeablepackage',
            unique_together=set([('upgrade_report', 'package_id')]),
        ),
        migrations.AddField(
            model_name='svainformation',
            name='virtual_machine',
            field=models.ForeignKey(to='dashboard.VirtualMachine'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='report',
            name='virtual_machine',
            field=models.ForeignKey(to='dashboard.VirtualMachine'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='definition',
            name='scan_report',
            field=models.ForeignKey(to='dashboard.ScanReport'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='definition',
            unique_together=set([('scan_report', 'definition_id')]),
        ),
    ]
