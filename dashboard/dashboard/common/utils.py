from django.core.files import File


def save_file(destination, content):
    with open(destination, 'w+') as f:
        f.write(content)
    return f
