import time
from managers import SVAInformationManager, ListReportManager, ExtendedScanReportManager, \
    ScanReportManager, UpReportManager, CveManager, VirtualMachineManager
from django.utils import timezone
from scanning_report_parser import ScanningReportGenerator
from upgrade_report_parser import UpgradeReportGenerator
from django.db import models
from django.db.models import Max


def generated_content(instance, filename):
    return '{0}/{1}/{2}'.format(instance.get_folder_name(), instance.virtual_machine.ip,
                                time.strftime(instance.get_folder_name() + '_%d_%m_%Y_%H_%M.json', time.localtime()))


def raw_content(instance, filename):
    return '{0}/{1}/{2}'.format(instance.get_folder_name(), instance.virtual_machine.ip,
                                time.strftime(instance.get_folder_name() + '_%d_%m_%Y_%H_%M.xml', time.localtime()))


def plan(instance, filename):
    return '{0}/{1}/{2}'.format(instance.get_folder_name(), instance.virtual_machine.ip,
                                time.strftime('plan_%d_%m_%Y_%H_%M.json', time.localtime()))


class VirtualMachine(models.Model):
    ip = models.IPAddressField()
    is_online = models.BooleanField(default=True)

    objects = VirtualMachineManager()


class SVAInformation(models.Model):
    virtual_machine = models.ForeignKey(VirtualMachine, on_delete=models.CASCADE)
    list_age_sva_msr2 = models.DateTimeField(null=True, verbose_name='List age')
    report_basic_age_sva_msr1 = models.DateTimeField(null=True, verbose_name='Basic scan age')
    up_report_age_sva_msr4 = models.DateTimeField(null=True, verbose_name='Upgrade report age')
    current_repository = models.CharField(max_length=100, null=True, verbose_name='Current repository')

    list_update_frequency = models.IntegerField(null=True, verbose_name='List update frequency')
    basic_scan_frequency = models.IntegerField(null=True, verbose_name='Basic scan frequency')
    extended_scan_frequency = models.IntegerField(null=True, verbose_name='Extended scan frequency')
    upgrade_report_frequency = models.IntegerField(null=True, verbose_name='Upgrade report frequency')

    list_availability_sva_msr7 = models.NullBooleanField(verbose_name='List available')
    up_report_availability_sva_msr10 = models.NullBooleanField(verbose_name='Upgrade report available')
    scan_report_availability_sva_msr9 = models.NullBooleanField(verbose_name='Scan report available')
    repository_availability_sva_msr6 = models.NullBooleanField(verbose_name='Repository available')
    scanner_availability_sva_msr8 = models.NullBooleanField(verbose_name='Scanner available')
    plan = models.FileField(null=True, upload_to=plan, verbose_name='Plan')
    objects = SVAInformationManager()

    @staticmethod
    def get_folder_name():
        return 'plan'


class Report(models.Model):
    GENERATING = 0
    FINISHED = 1
    TERMINATED = 2
    STATUS_CHOICES = (
        (GENERATING, "Generating"),
        (FINISHED, "Finished"),
        (TERMINATED, "Terminated"),
    )
    virtual_machine = models.ForeignKey(VirtualMachine, on_delete=models.CASCADE)
    time_added = models.DateTimeField(auto_now_add=True)
    task_id = models.CharField(max_length=200)
    status = models.SmallIntegerField(choices=STATUS_CHOICES, default=GENERATING)
    raw_content = models.FileField(null=True, upload_to=raw_content)

    def get_total_seconds(self):
        return (timezone.now() - self.time_added).total_seconds()

    def get_status(self, frequency):
        f = int(frequency) * 3600
        if self.get_total_seconds() > f:
            return 'VIOLATION'
        return 'OK'


class ExtendedScanReport(Report):
    objects = ExtendedScanReportManager()

    @staticmethod
    def get_folder_name():
        return 'extended_scan'


class ListReport(Report):
    objects = ListReportManager()

    @staticmethod
    def get_folder_name():
        return 'list_update'


class UpgradeReport(Report):
    objects = UpReportManager()

    def generate_report(self, results, available_updates):
        return UpgradeReportGenerator.generate_upgrade_report(self, results, available_updates)

    @staticmethod
    def get_folder_name():
        return 'upgrade_report'


class ScanReport(Report):
    objects = ScanReportManager()

    def generate_report(self, results):
        ScanningReportGenerator.generate_scanning_report(self, results)

    def get_vulnerability_size(self):
        return self.definition_set.count()

    def get_max_cvss(self):
        return self.definition_set.aggregate(max_cvss=Max('cve__cvss'))['max_cvss']

    @staticmethod
    def get_folder_name():
        return 'scan_report'


class Cve(models.Model):
    cve = models.CharField(max_length=128, db_index=True)
    cvss = models.FloatField(null=True)
    objects = CveManager()


class Definition(models.Model):
    scan_report = models.ForeignKey(ScanReport)
    definition_id = models.CharField(max_length=512, db_index=True)
    description = models.CharField(max_length=2048)
    cve = models.ForeignKey(Cve, null=True)
    vulnerability_result = models.NullBooleanField()

    class Meta:
        unique_together = (("scan_report", "definition_id"),)


class Package(models.Model):
    definitions = models.ManyToManyField(Definition, null=True)
    package_id = models.CharField(max_length=128)
    name = models.CharField(max_length=1024)
    version = models.CharField(max_length=128)
    available_version = models.CharField(max_length=128)


class UpgradeablePackage(models.Model):
    upgrade_report = models.ForeignKey(UpgradeReport)
    package_id = models.CharField(max_length=128)
    name = models.CharField(max_length=1024)
    current_version = models.CharField(max_length=128)
    available_version = models.CharField(max_length=128)

    class Meta:
        unique_together = (("upgrade_report", "package_id"),)
